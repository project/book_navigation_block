<?php

namespace Drupal\book_navigation_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Url;
use Drupal\node\Entity\Node;
use Drupal\node\NodeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Provides a 'Book Navigation' Block.
 *
 * @Block(
 *   id = "book_navigation_block",
 *   admin_label = @Translation("Book Navigation Block"),
 * )
 */
class BookNavigationBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * Constructs a new BookNavigationBlock instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   *   The book manager service.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The current route match.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, RouteMatchInterface $route_match) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->routeMatch = $route_match;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_route_match'),
    );
  }

 /**
   * Builds the book navigation block.
   *
   * @return array
   *   A render array as expected by Drupal's rendering system.
   */
  public function build() {
    $node = $this->routeMatch->getParameter('node');
    $isNode = $node instanceof NodeInterface;
    $build_array = [];
    if ($isNode && !empty($node->book)) {
      // Only proceed if the node type is allowed in book settings
      $allowed_types = \Drupal::config('book.settings')->get('allowed_types');
      $isBookType = in_array($node->getType(), $allowed_types);
      if ($isBookType) {
        $navigation_links = $this->generateNavigationLinks($node);

        if (!empty($navigation_links)) {
          $build_array = [
            '#theme'            => 'book_navigation_block',
            '#navigation_links' => $navigation_links,
            '#cache'            => [
              'contexts' => [
                'url.path',
              ],
              'tags'     => [
                'node:' . $node->id(),
              ],
            ],
          ];
        }
      }
    }

    return $build_array;
  }

  /**
   * Generates navigation links for the book.
   *
   * This method creates previous and next navigation links for the book
   * based on the current node.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The current node entity.
   *
   * @return array
   *   An associative array containing the navigation links.
   */
  private function generateNavigationLinks(NodeInterface $node) {
    $navigation_links = [];
    /** @var \Drupal\book\BookOutline $book_outline */
    $book_outline = \Drupal::service('book.outline');
    $book_link = $node->book;

    // Previous Link
    $prev_link = $book_outline->prevLink($book_link);
    if ($prev_link) {
      $navigation_links['prev'] = $this->generateNodeLink($prev_link);
    }

    // Top link
    $parent_nid = $book_link['pid'];
    // If this is the top, then the pid will be zero.
    if (is_numeric($parent_nid) && $parent_nid) {
      $top_link = [
        'nid' => $parent_nid,
        'title' => Node::load($parent_nid)->getTitle(),
      ];
      $navigation_links['top'] = $this->generateNodeLink($top_link);
    }

    // Next Link
    $next_link = $book_outline->nextLink($book_link);
    if ($next_link) {
      $navigation_links['next'] = $this->generateNodeLink($next_link);
    }

    return $navigation_links;
  }

  /**
   * Generates a link array for a given book link.
   *
   * @param array $book_link
   *   The book link array.
   *
   * @return array|null
   *   An associative array containing 'url' and 'title' of the node link,
   *   or null if the node ID is invalid.
   */
  private function generateNodeLink($book_link) {
    $nid = isset($book_link['nid']) ? $book_link['nid'] : null;

    // Ensure that the node ID is numeric and not empty.
    if (!empty($nid) && is_numeric($nid)) {
      $url = Url::fromRoute('entity.node.canonical', ['node' => $nid])->toString();
      return [
        'url'   => $url,
        'title' => $book_link['title'],
      ];
    }

    return null;
  }

}